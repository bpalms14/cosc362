#!/bin/bash

HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=4
BACKTITLE="Your Options Back Title"
TITLE="Big Time Options"
MENU="You Must Choose!"

OPTIONS=(1 "Option 1"
	 2 "Option 2"
	 3 "Option 3")

CHOICE=$(dialog --clear \
		--backtitle "$BACKTITLE" \
		--title "$TITLE" \
		--menu "$MENU" \
		--$HEIGHT $WIDTH $CHOICE_HEIGHT \
		--"${OPTIONS[@]}" \
		2>&1 >/dev/tty)

clear
case $CHOICE in
	1)
    		echo "You chose option 1"
		;;
	2)
		echo "You chose option 2"
		;;
	3)
		echo "Git smack yeeeeeeeeeeeet option 3"
		ls
		echo "We took your files"
		;;
esac
