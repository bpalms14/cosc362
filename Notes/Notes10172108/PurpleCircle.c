#include <stdio.h>
#include <stdlib.h>
#include "Myfunctions.h"
/*
* This is a c program that does something cool.
* Many line comment.
*/

int main(int argc,  char *argv[]){
	
	int numRows; 
	int numCols;
	int imageSize;
	int row, col;
	int radius;
	int InOut; /* flag where (0 = out , and 1 = circle) */
	unsigned char *outImage;
	unsigned char *ptr;
	unsigned char *outputFP;
	printf("===============================\n"); //this is a single line comment.
	printf("I'm making a Plain Pixel Map!  \n");
	printf("===============================\n\n");

	if(argc!=4){
	   print("Usage: ./RedBluePPM OUTFileName numrow numcols \n");
	   exit(1);
	}
	if ( (numRows = atoi(argv[2])) <= 0){
	    printf("Error: numRows needs to be positive");
	}
	if ( (numCols = atoi(argv[3]) ) <= 0){
		printf("Error: numCols needs to be positive");
        }
	if ( (radius = atoi(argv[4]) ) <= 0){
		printf("Error: the radius shoule be postive"):
	}
	// ====================================================
	// Set up space for my soon to be ppm image.
	// ====================================================
	imageSize = numRows*numCols*3;
	outImage  = (unsigned char *) malloc(imageSize); //get enough space for my image.
	
	/* Open a file to pu the output image into */
	if (outputFP =  fopen = (argv[1], "w")) == NULL){
	   perror("ouput open error");
	   printf("Error: can not open output file\n");
	   exit(1);
}

	/* Now lets create the plain pixel map!!!*/
	ptr = outImage;
	for(row = 0; row < numRows; row++){
	    for(col = 0; col < numCols; col++){
	    // Walk through each row of the  image column by column.
	 InOut = InCircle(numRows,numCols,radius,row,col); 

	 if col < numCols/2){
		/* Red Pixel */
		*ptr = 255;
		*(ptr+1) = 0;
		*(ptr+2) = 0; 
	   }
	   else {
		*ptr     = 0;
		*(ptr+1) = 0;
		*(ptr+2) = 255;
	}
		// Advance the pointer
		ptr += 3;
	}
    }

	fprintf(outputFP, "P6 %d %d 255\n", numCols, numRows);
	fwrite(outImage, 1 imageSize, outputFP);

	/* Done */
	fclose(outputFP);
 	return 0;

}

