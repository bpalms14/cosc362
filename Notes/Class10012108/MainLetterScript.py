from LetterFunctions import *

""" This is a script to do a mail merege to make custome letters to magic beings.
    To Run: 
    [user@machine directory]$ python MainLetterScript.py Mynamesaddresses.csv
"""

# ===========================================
# READ IN COMMAND LINE ARGUMENTS
# ===========================================
if len(sys.argv) !=2:
	print( "TO Run: python MainLetterScript.py Addresslist.txt" )
else:
	print ("Using address from the file", str(sys.argv[1]))
	dataFile = str(sys.argv[1])
# ============================================
# OPEN FILES
# ===========================================
AddressData=open(dataFile,"r")

# ===========================================
# READ DATA FROM FILE LINE BY LINE
# ===========================================
print "Reading from a file" + dataFile

AddressList= []
for line in Addressdata.readlines():
    First, Last, Address1, Address2 = map(str,line.split(',')
    CurrentAddress = Address(First, Last, Address1, Address2)
    AddressList.append(CurrentAddress)
    CurrentAddress.dumpAddress()

# =========================================
# WRITE OUT THE FORM LETTERS
# =========================================
for i in range(len(AddressList)):
    Letter = DocumentText(AddressList[i])
    Letter.WriteLetter()
