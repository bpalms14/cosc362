#!/bin/bash 

# hopefull we will read some parameters for this script from the command line.

echo "To use: ./ArgumentsEx.sh Thing1 Thing2 Thing3."

POSPAR1="$1"
POSPAR2="$2"
POSPAR3="$3"

echo "$1 is the first postion parameter. \$1."
echo "$2 is the second postion parameter. \$2."
echo "$3 is the third postion paramerter. \$3."
echo
echo "The total number of parameters is given by $#."
