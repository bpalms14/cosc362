/* This is a bunch of stuff to use in pixel mapping. */ 
#include <math.h> // Allows me to use the pow(x,y)  ---> x^y

int InCircle(int totalrows, int totalcols, int radius, int pixRow, int pixCol){
    int InOrOut = 0; // Integer flag for if the current pixel is in the circle or not. 
                     // 1 ==> yes, 0 ==> no.

    int Centerx = totalcols/2;
    int Centery = totalrows/2;
    int dist    = 0;            //Distance from center to pixel.

    dist = pow((pow(Centerx - pixCol,2))+(pow(Centery - pixRow,2)),0.5);

    if (dist < radius){
        InOrOut = 1;
    }

    return InOrOut;
}