/* This is a bunch of stuff to use in pixel mapping. */ 
#include <math.h> // Allows me to use the pow(x,y)  ---> x^y

int InCircle(int totalrows, int totalcols, int radius, int pixRow, int pixCol){
    int InOrOut = 0; // Integer flag for if the current pixel is in the circle or not. 
                     // 1 ==> yes, 0 ==> no.

    int Centerx = totalcols/2;
    int Centery = totalrows/2;
    int dist    = 0;            //Distance from center to pixel.

    dist = pow((pow(Centerx - pixCol,2))+(pow(Centery - pixRow,2)),0.5);

    if (dist < radius){
        InOrOut = 1;
    }

    return InOrOut;
}

int cuts(int totalrows, int totalcols, int radius, int pixRow, int pixCol){
   int holes = 0;

   int Centerx = totalcols/2;
   int Centery = totalrows/2;  
   int dist = 0;

   dist = (pow((pow(Centerx - pixCol,2))+(pow(Centery - pixRow,2)),0.5)-20);

   if (((pixCol > 400 && pixCol < 450 && pixRow > 400 && pixRow < 450)) ||
	(pixCol > 600 && pixCol < 650 && pixRow > 400 && pixRow < 450) ||
	(pixCol > 400 && pixCol < 650 && pixRow > 550 && pixRow < 600) ||
	(pixCol > 500 && pixCol < 550 && pixRow > 450 && pixRow < 500)){

	holes = 1;
   }

    return holes;

}

int stem(int totalrows, int totalcols, int radius, int pixRow, int pixCol){

   int top = 0;

   int Centerx = totalcols/2;
   int Centery = totalrows/2;



   if (pixCol > 470 && pixCol < 550 && pixRow > 250 && pixRow < 320 ){

	top = 1;

   }

   return top;
}

