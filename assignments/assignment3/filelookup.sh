#!/bin/bash

file='*.tex'
wc='*.pdf'

for eachfile in $file
do
	pdflatex $eachfile
	rm *.bib *.log *.aux *.out
	for i in $wc
		 do
		 wc i >wordcount.txt
	done
done


